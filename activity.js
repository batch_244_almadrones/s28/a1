// Item 3
db.hotels.insertOne({
  name: "single",
  accomodates: 2,
  price: 1000,
  description: "A Simple room with all the basic necessities.",
  roomsAvailable: 10,
  isAvailable: false,
});

// Item 4
db.hotels.insertMany([
  {
    name: "double",
    accomodates: 3,
    price: 2000,
    description: "A room fit for a small family going on a vacation.",
    roomsAvailable: 5,
    isAvailable: false,
  },
  {
    name: "queen",
    accomodates: 4,
    price: 4000,
    description: "A room with a queen sized bed perfect for a simple getaway.",
    roomsAvailable: 15,
    isAvailable: false,
  },
]);

// Item 5
db.hotels.find({ name: "double" });

//Item 6
db.hotels.updateOne(
  { name: "queen" },
  {
    $set: {
      roomsAvailable: 0,
    },
  }
);

// Item 7
db.hotels.deleteMany({ roomsAvailable: 0 });
